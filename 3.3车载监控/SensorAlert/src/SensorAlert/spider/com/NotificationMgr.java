package SensorAlert.spider.com;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.RemoteViews;


public class NotificationMgr {
	private Notification notification=null;
	private NotificationManager mNotificationManager=null;
	private Context context;
	
	public NotificationMgr(Context context){
		this.context=context;
	}
	public void useNotifi(String title, String content) {
		String ns = Context.NOTIFICATION_SERVICE;
		mNotificationManager = (NotificationManager) context .getSystemService(ns);
		notification = new Notification();
		notification.icon = R.drawable.icon;
		//notification.tickerText =getApplication().getString(R.string.tempAlarm);
		notification.when = System.currentTimeMillis();
		RemoteViews contentView = new RemoteViews(context
				.getApplicationContext().getPackageName(),
				R.layout.notification);
		notification.contentView = contentView;
		Intent notificationIntent = new Intent(context, MainActivity.class);  
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
				notificationIntent, 0);
		notification.setLatestEventInfo(context, title, content,contentIntent);
		notification.contentIntent = contentIntent;
		mNotificationManager.notify(0, notification);
	}
	
	public void clear(){
		String ns = Context.NOTIFICATION_SERVICE;
		mNotificationManager = (NotificationManager) context
				.getSystemService(ns);
		mNotificationManager.cancelAll();
	}	
}