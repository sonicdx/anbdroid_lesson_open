package com.dianzi.drawview_init;

import java.util.Calendar;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

public class Lesson_Codes {
	/**
	 * 背景色  PPT:23
	 * @param canvas
	 */
	public static void  Demo_fillColor(Canvas canvas)
	{
		 canvas.drawColor(Color.BLUE);
	}
	
	/**
	 *  画线  PPT:26
	 * @param canvas
	 */
	public static void  Demo_drawLines(Canvas canvas)
	{
		//super.onDraw(canvas);
		canvas.drawColor(Color.BLUE);
		Paint linep = new Paint();
		linep.setStrokeWidth(5);
		linep.setColor(Color.RED);
		canvas.drawLine(0, 0, 300, 300, linep);
			
		linep.setColor(Color.GREEN);
		canvas.drawLine(300, 0, 0, 300, linep);
	}
	
	/**
	 *  画圆  PPT:27
	 * @param canvas
	 */
	public static void  Demo_drawCircle(Canvas canvas)
	{
		//super.onDraw(canvas);
		canvas.drawColor(Color.BLUE);
		
		Paint linep = new Paint();
		linep.setStrokeWidth(5);
		linep.setColor(Color.RED);
		linep.setStyle(Paint.Style.FILL);
		canvas.drawCircle(200, 200, 100, linep);
		
		linep.setStyle(Paint.Style.STROKE);
		canvas.drawCircle(200, 400, 100, linep);
	}
	
	/**
	 *  封闭多边形  PPT:32
	 * @param canvas
	 */
	public static void  Demo_drawClosedLines(Canvas canvas)
	{
		canvas.drawColor(Color.BLUE);
		
		Paint p = new Paint();
		p.setColor(Color.RED);
		p.setStrokeWidth(4);
		p.setStyle(Paint.Style.STROKE);
		
		Path path = new Path();
		path.moveTo(80, 500);
		path.lineTo(320, 250);
		path.lineTo(80, 150);
		path.close(); 
		canvas.drawPath(path, p);
	}
	
	/**
	 * 非封闭多边形  PPT:33
	 * @param canvas
	 */
	public static void  Demo_drawNotClosedLines(Canvas canvas)
	{
		canvas.drawColor(Color.BLUE);
		
		Paint p = new Paint();
		p.setColor(Color.RED);
		p.setStrokeWidth(4);
		p.setStyle(Paint.Style.STROKE);
		
		Path path = new Path();
		path.moveTo(80, 500);
		path.lineTo(320, 250);
		path.lineTo(80, 150);
		//path.close(); 
		canvas.drawPath(path, p);
	}
	
	/***
	 * 描边： 宽度与颜色   PPT:35
	 * @param canvas
	 */
	public static void  Demo_drawLinesColorAndWidth(Canvas canvas)
	{
		//super.onDraw(canvas);
		canvas.drawColor(Color.BLUE);
		
		Paint p = new Paint();
		p.setColor(Color.RED);
		p.setStrokeWidth(4);
		p.setStyle(Paint.Style.STROKE);

		canvas.drawLine(10, 50, 210, 50, p);
		p.setStrokeWidth(15);
		canvas.drawLine(10, 150, 210, 150, p);
		p.setColor(Color.GREEN);
		canvas.drawLine(10, 250, 210, 250, p);
	}
	
	/**
	 * 触摸处理    PPT:44-45
	 * @author 御
	 */
	public class DemoTouchEvent
	{
		int posX = -1;
		int posY = -1;
		boolean isTouching = false;
		
		protected void onDraw(Canvas canvas) {
			//super.onDraw(canvas);
			canvas.drawColor(Color.BLUE);
			
			Paint p = new Paint();
			p.setTextSize(18);
			if(isTouching == true)
			{
				canvas.drawText("当前X:" + posX, 0, 30, p);
				canvas.drawText("当前Y:" + posY, 0, 50, p);
			}
		}
		
		  public boolean onTouchEvent(MotionEvent event) {
		        switch (event.getAction()) {
		            case MotionEvent.ACTION_DOWN:
		            	isTouching = true;
		                invalidate();
		                break;
		            case MotionEvent.ACTION_MOVE:
		        		posX = (int) event.getX();
		        		posY = (int) event.getY();
		                invalidate();
		                break;
		            case MotionEvent.ACTION_UP:
		            	isTouching = false;
		                invalidate();
		                break;
		        }
		        return true;
			}
		  
		  private void invalidate(){};
	}
	
	/**
	 * 习题2：否实现利用一个位图资源绘制一个背景图片？
	 * @author 御
	 */
	public class DemoDrawImageView extends View
	{
		public DemoDrawImageView(Context context, AttributeSet attrs) {
			super(context, attrs);
			// TODO Auto-generated constructor stub
		}
		Bitmap logoImg = BitmapFactory.decodeResource(getResources(), com.dianzi.drawview_init.R.drawable.logo);
		protected void onDraw(Canvas canvas) {
			//super.onDraw(canvas);
			canvas.drawColor(Color.BLUE);
			Paint p = new Paint();
			canvas.drawBitmap(logoImg, 0, 0, p);
		}
	}
	
	/**
	 * 习题3：请设计一个函数，其功能是在画板上画出一个正n边形来，n有程序内一个变量控制
	 * @author 御
	 */
	public class DrawNPolyonView extends View
	{

		public DrawNPolyonView(Context context, AttributeSet attrs) {
			super(context, attrs);

		}

		int PolyCount = 0;
		Bitmap logoImg = BitmapFactory.decodeResource(getResources(), com.dianzi.drawview_init.R.drawable.logo);
		@Override
		protected void onDraw(Canvas canvas) {
			//super.onDraw(canvas);
			canvas.drawColor(Color.BLUE);
			
			Paint p = new Paint();
			DrawNPolyon(this.getWidth() / 2,this.getHeight() / 2,PolyCount + 3,150,getContext(),canvas);
		}

		
		boolean isTouching = false;
		@Override
		  public boolean onTouchEvent(MotionEvent event) {
	        switch (event.getAction()) {
	            case MotionEvent.ACTION_DOWN:
	            	isTouching = true;
	            	PolyCount = (PolyCount + 1) % 20;
	                invalidate();
	                break;
	            case MotionEvent.ACTION_MOVE:
	                invalidate();
	                break;
	            case MotionEvent.ACTION_UP:
	            	isTouching = false;
	                invalidate();
	                break;
	        }
	        return true;
		}
		
		protected void DrawNPolyon(int x,int y,int n,int r,Context caller,Canvas drawer)
		{
			if(n < 3)
			{
				Toast.makeText(caller, "不能绘制边数小于3的多边形", Toast.LENGTH_SHORT).show();
				return;
			}
			
			Paint p = new Paint();
			p.setStrokeWidth(4);
			p.setColor(Color.RED);
			double perAng = Math.PI * 2 / n;
			for(int i=0;i<n;i++)
			{
				int startIndex = i;
				int endIndex = (i + 1) % n;
				float startX = (float)x + (float)(r * Math.sin(startIndex * perAng));
				float startY = (float)y + (float)(r * Math.cos(startIndex * perAng));
				float stopX = (float)x + (float)(r * Math.sin(endIndex * perAng));
				float stopY = (float)y + (float)(r * Math.cos(endIndex * perAng));
				drawer.drawLine(startX, startY, stopX, stopY, p);
			}	
		}
	}
	
	/**
	 * 习题4：如何在画板上实现一个模拟时钟的动画
	 * @author 御
	 */
	public class DrawTimerView extends View
	{
		ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
		public DrawTimerView(Context context, AttributeSet attrs) {
			super(context, attrs);
			// TODO Auto-generated constructor stub
			final Runnable task = new Runnable() { 
	            @Override  
	            public void run() {  
	            	postInvalidate();  
	            } 
			};
			//启动动画
			scheduler.scheduleWithFixedDelay(task, 100 * 5, 100 * 5, TimeUnit.MILLISECONDS);
		}
		
		@Override
		protected void onDraw(Canvas canvas) {
			//super.onDraw(canvas);
			canvas.drawColor(Color.BLUE);
			Paint p = new Paint();
			
			float centerx = this.getWidth() / 2;
			float centery = this.getHeight() / 2;
			
			Calendar now=Calendar.getInstance();
			int sec = now.get(Calendar.SECOND);
			float secx = (float)Math.sin(Math.PI - ((Math.PI * 2) / 60.0) * sec) * 200 + centerx;
			float secy = (float)Math.cos(Math.PI - ((Math.PI * 2) / 60.0) * sec) * 200 + centery;
			
			int minute = now.get(Calendar.MINUTE);
			float minutex = (float)Math.sin(Math.PI - ((Math.PI * 2) / 60.0) * minute) * 150 + centerx;
			float minutey = (float)Math.cos(Math.PI - ((Math.PI * 2) / 60.0) * minute) * 150 + centery;
		
			int hour = now.get(Calendar.HOUR);
			float hourx = (float)Math.sin(Math.PI * 2 - ((Math.PI * 2) / 12.0) * hour + Math.PI) * 100 + centerx;
			float houry = (float)Math.cos(Math.PI * 2 - ((Math.PI * 2) / 12.0) * hour + Math.PI) * 100 + centery;
			
			p.setStrokeWidth(15);
			p.setColor(Color.GREEN);
			canvas.drawLine(centerx, centery, hourx, houry, p);
			p.setStrokeWidth(8);
			p.setColor(Color.LTGRAY);
			canvas.drawLine(centerx, centery, minutex, minutey, p);
			p.setStrokeWidth(4);
			p.setColor(Color.RED);
			canvas.drawLine(centerx, centery, secx, secy, p);
			
			p.setStrokeWidth(15);
			p.setColor(Color.WHITE);
			canvas.drawCircle(centerx, centery, 15, p);
		}
	}
	
	/**
	 * 习题5：请在画板上，绘制四个200像素宽度的矩形，并按下图摆放，使用触摸功能，让这四个方块实现按钮的触摸样式，也就是当手指按在方块上时，方块填充蓝色；当放开时，填充为红色
	 * @author 御
	 */
	public class ClickRectView extends View
	{

		public ClickRectView(Context context, AttributeSet attrs) {
			super(context, attrs);

		}
		
		RectF selRect = null;
		@Override
		protected void onSizeChanged(int w, int h, int oldw, int oldh) {
			// TODO Auto-generated method stub
			super.onSizeChanged(w, h, oldw, oldh);
			
			selRect = null;
			if(w == 0)
				return;
			
			selRect = new RectF(w/4,h/4,w/4 * 3,h/4 * 3);
			this.invalidate();
		}

		@Override
		protected void onDraw(Canvas canvas) {
			//super.onDraw(canvas);
			canvas.drawColor(Color.WHITE);
			

			if(selRect != null)
			{
				Paint p = new Paint();
				if(isInRect == true)
					p.setColor(Color.BLUE);
				else
					p.setColor(Color.RED);
				
				p.setStyle(Style.FILL);
				canvas.drawRect(selRect, p);
			}
		}

		boolean isTouching = false;
		boolean isInRect = false;
		@Override
		  public boolean onTouchEvent(MotionEvent event) {
	        switch (event.getAction()) {
	            case MotionEvent.ACTION_DOWN:
	            	isTouching = true;
	            	CalIsInRect(event.getX(),event.getY());
	                invalidate();
	                break;
	            case MotionEvent.ACTION_MOVE:
	            	CalIsInRect(event.getX(),event.getY());
	                invalidate();
	                break;
	            case MotionEvent.ACTION_UP:
	            	isTouching = false;
	            	isInRect = false;
	                invalidate();
	                break;
	        }
	        return true;
		}
		
		private void CalIsInRect(float mouseX,float mouseY)
		{
			if(selRect == null)
				isInRect = false;
			
			isInRect = selRect.contains(mouseX, mouseY);
		}
	}
	/**
	 * 习题6：请在画板上，绘制四个200像素宽度的蓝色矩形，请实现用手指能拖动它的功能
	 * @author 御
	 */
	public class DragRectView extends View
	{
		public DragRectView(Context context, AttributeSet attrs) {
			super(context, attrs);

		}
		
		RectF selRect = null;
		@Override
		protected void onSizeChanged(int w, int h, int oldw, int oldh) {
			// TODO Auto-generated method stub
			super.onSizeChanged(w, h, oldw, oldh);
			
			selRect = null;
			if(w == 0)
				return;
			
			selRect = new RectF(w/2 - 100,h/2 - 100,w/2 + 100,h/2 + 100);
			this.invalidate();
		}

		@Override
		protected void onDraw(Canvas canvas) {
			//super.onDraw(canvas);
			canvas.drawColor(Color.WHITE);
			

			if(selRect != null)
			{
				Paint p = new Paint();
				if(isTouching == true)
					p.setColor(Color.BLUE);
				else
					p.setColor(Color.RED);
				
				p.setStyle(Style.FILL);
				canvas.drawRect(selRect, p);
			}
		}

		
		boolean isTouching = false;
		boolean isInRect = false;
		@Override
		  public boolean onTouchEvent(MotionEvent event) {
	        switch (event.getAction()) {
	            case MotionEvent.ACTION_DOWN:
	            	isTouching = isCalIsInRect(event.getX(),event.getY());
	                invalidate();
	                break;
	            case MotionEvent.ACTION_MOVE:
	            	if(isTouching == true)
	            	{
	                	selRect.left = event.getX() - 100;
	                	selRect.top = event.getY() - 100;
	                	selRect.right = event.getX() + 100;
	                	selRect.bottom = event.getY() + 100;
	                    invalidate();
	            	}
	                break;
	            case MotionEvent.ACTION_UP:
	            	isTouching = false;
	                invalidate();
	                break;
	        }
	        return true;
		}
		
		private boolean isCalIsInRect(float mouseX,float mouseY)
		{
			if(selRect == null)
				return false;
			
			return selRect.contains(mouseX, mouseY);
		}
	}
}
