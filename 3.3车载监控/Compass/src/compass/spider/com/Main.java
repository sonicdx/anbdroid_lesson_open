package compass.spider.com;

import java.util.List;

import compass.spider.com.R;
import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;


public class Main extends Activity  implements SensorEventListener
{
	private ImageView imageView;
	private float currentDegree = 0f;
    private SensorManager  mSM; 

    private boolean   mRegisteredSensor;
  
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.main);

		imageView = (ImageView) findViewById(R.id.imageview);	
		mSM= (SensorManager) getSystemService(SENSOR_SERVICE); 
		float scale = getApplicationContext().getResources().getDisplayMetrics().density; 

		
	}
	
	 @Override
     public void onResume(){  
      super.onResume();  
      List<Sensor> sensors = mSM.getSensorList(Sensor.TYPE_ALL);
      for (Sensor s : sensors) { 
                mRegisteredSensor = mSM.registerListener((SensorEventListener) this, s, SensorManager.SENSOR_DELAY_FASTEST);
             }
      
     }
     
     @Override
     public void onPause(){
       if (mRegisteredSensor){ //注销Listener
                mSM.unregisterListener(this);
                mRegisteredSensor = false;
             }
      super.onPause();
     }

	 

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		
		if (event.sensor.getType()== (Sensor.TYPE_ORIENTATION))
		{  	
			float degree = event.values[0];	
		   // Method 1, use relative central point to rotate
		    RotateAnimation ra = new RotateAnimation(currentDegree, -degree,
			Animation.RELATIVE_TO_SELF, 0.5f,
			Animation.RELATIVE_TO_SELF, 0.5f);
			
		    //Method 2, use absolute central point to rotate
		    //RotateAnimation ra = new RotateAnimation(currentDegree, -degree,69,72);
			ra.setDuration(200);
					
			imageView.startAnimation(ra); 
			currentDegree = -degree;
		}	
	}
	 
}