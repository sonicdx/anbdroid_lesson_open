package com.dianzi.drawview_exs;

import java.util.Calendar;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class EX4_VIEW extends View {
	ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
	public EX4_VIEW(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		final Runnable task = new Runnable() { 
            @Override  
            public void run() {  
            	postInvalidate();  
            } 
		};
		//��������
		scheduler.scheduleWithFixedDelay(task, 100 * 5, 100 * 5, TimeUnit.MILLISECONDS);
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		//super.onDraw(canvas);
		canvas.drawColor(Color.BLUE);
		Paint p = new Paint();
		
		float centerx = this.getWidth() / 2;
		float centery = this.getHeight() / 2;
		
		Calendar now=Calendar.getInstance();
		int sec = now.get(Calendar.SECOND);
		float secx = (float)Math.sin(Math.PI - ((Math.PI * 2) / 60.0) * sec) * 200 + centerx;
		float secy = (float)Math.cos(Math.PI - ((Math.PI * 2) / 60.0) * sec) * 200 + centery;
		
		int minute = now.get(Calendar.MINUTE);
		float minutex = (float)Math.sin(Math.PI - ((Math.PI * 2) / 60.0) * minute) * 150 + centerx;
		float minutey = (float)Math.cos(Math.PI - ((Math.PI * 2) / 60.0) * minute) * 150 + centery;
	
		int hour = now.get(Calendar.HOUR);
		float hourx = (float)Math.sin(Math.PI * 2 - ((Math.PI * 2) / 12.0) * hour + Math.PI) * 100 + centerx;
		float houry = (float)Math.cos(Math.PI * 2 - ((Math.PI * 2) / 12.0) * hour + Math.PI) * 100 + centery;
		
		p.setStrokeWidth(15);
		p.setColor(Color.GREEN);
		canvas.drawLine(centerx, centery, hourx, houry, p);
		p.setStrokeWidth(8);
		p.setColor(Color.LTGRAY);
		canvas.drawLine(centerx, centery, minutex, minutey, p);
		p.setStrokeWidth(4);
		p.setColor(Color.RED);
		canvas.drawLine(centerx, centery, secx, secy, p);
		
		p.setStrokeWidth(15);
		p.setColor(Color.WHITE);
		canvas.drawCircle(centerx, centery, 15, p);
	}
}
