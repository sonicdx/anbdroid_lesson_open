package com.dianzi.drawview_init;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

public class InitView extends View   {
	private int selColor = Color.BLACK;

	private int cavW = 0;
	private int cavH = 0;

    
	public InitView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
	    canvas.drawColor(selColor);
	}

	public int getSelColor() {
		return selColor;
	}

	public void setSelColor(int selColor) {
		this.selColor = selColor;
		this.invalidate();
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		// TODO Auto-generated method stub
		super.onSizeChanged(w, h, oldw, oldh);
		
		this.cavW = w;
		this.cavH = h;
	}
}
