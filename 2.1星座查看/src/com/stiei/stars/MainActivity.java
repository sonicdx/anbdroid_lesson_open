package com.stiei.stars;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends Activity {

	private Spinner sp1;
	private DatePicker dp1;
	private Button bt1;
	private Button bt2;
	private static final String[ ] stars={"������","��ţ��","˫����","��з��",
		                                                "ʨ����","��Ů��","�����","��Ы��",
		                                                "������","Ħ����","ˮƿ��","˫����"}; 
	private ArrayAdapter<String> adapter;
	private long selectno;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		sp1 = (Spinner) findViewById(R.id.spinner1);
		dp1 = (DatePicker) findViewById(R.id.datePicker1);
		bt1 = (Button)findViewById(R.id.button1);
		bt2 = (Button)findViewById(R.id.button2);
	
		bt1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,stars);
	//	adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,stars);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); 
		sp1.setAdapter(adapter);//
		
		
		dp1.init(2013, 2,21 , new DatePicker.OnDateChangedListener() {
			
			@Override
			public void onDateChanged(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {
				// TODO Auto-generated method stub
				switch(monthOfYear)
				{
				  case 0:if(dayOfMonth<20)
			             sp1.setSelection(9);
		                    else
			             sp1.setSelection(10);
		                 break;
		         case 1:if(dayOfMonth<19)
		                 sp1.setSelection(10);
	                        else
		                sp1.setSelection(11);
		                 break;
				  case 2:if(dayOfMonth<21)
					      sp1.setSelection(11);
				         else
				    	  sp1.setSelection(0);
				         break;
				         
				  case 3:if(dayOfMonth<20)
				          sp1.setSelection(0);
			             else
			    	      sp1.setSelection(1);
			             break;
				  case 4:if(dayOfMonth<21)
                      sp1.setSelection(1);
                   else
                      sp1.setSelection(2);
                         break;
			     case 5:if(dayOfMonth<22)
                     sp1.setSelection(2);
			          else
                      sp1.setSelection(3);
                         break;
			   case 6:if(dayOfMonth<23)
			            sp1.setSelection(3);
		              else
			             sp1.setSelection(4);
		                break;
		       case 7:if(dayOfMonth<23)
		                sp1.setSelection(4);
	                  else
		                 sp1.setSelection(5);
		                break;
		       case 8:if(dayOfMonth<23)
	                     sp1.setSelection(5);
                   else
	                     sp1.setSelection(6);
	                   break;
		       case 9:if(dayOfMonth<24)
                       sp1.setSelection(6);
                         else
                       sp1.setSelection(7);
                      break;
		       case 10:if(dayOfMonth<23)
                      sp1.setSelection(7);
                   else
                     sp1.setSelection(8);
            break;
		       case 11:if(dayOfMonth<22)
                     sp1.setSelection(8);
                   else
                      sp1.setSelection(9);
				
				}
				
			}
		});
		
		bt1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				selectno=sp1.getSelectedItemId();
				Intent intent = new Intent();
				intent.putExtra("selection", selectno);
				intent.setClass(MainActivity.this, starinfoActivity.class);
				startActivity(intent);
			}
		});
		
bt2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog aboutDg;
			    aboutDg=new AlertDialog.Builder(MainActivity.this)
			                                          .setTitle("����")
			    		                              .setMessage("������鿴 V1.2\nSTIEI Android CLUB")
			    		                              .create();
				aboutDg.show();
		//		Toast.makeText(getApplicationContext(), "ok", BIND_AUTO_CREATE).show();
			    
			}
		});
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	

}
