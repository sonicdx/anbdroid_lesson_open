package com.dianzi.drawview_init;

import yuku.ambilwarna.AmbilWarnaDialog;
import yuku.ambilwarna.AmbilWarnaDialog.OnAmbilWarnaListener;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Color;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class Init_DrawActivity extends Activity {
	private Button btuChangeColor = null;
	private SeekBar seekLineWidth = null;
	private TextView txtColorTip = null;
	private  InitView drawDes = null;
	
	private int selColor = Color.BLACK; 
	private Activity activity = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_init__draw);
	    activity = this;
		 
		btuChangeColor = (Button) findViewById(R.id.btuChangeColor);
		seekLineWidth = (SeekBar) findViewById(R.id.seekLineWidth);
		txtColorTip = (TextView)findViewById(R.id.color_tip); 
		drawDes = (InitView)findViewById(R.id.initView);
		
		txtColorTip.setBackgroundColor(selColor);
		btuChangeColor.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				colorpicker();
			}
		});  
		seekLineWidth.setMax(19);
		seekLineWidth.setProgress(5);
		seekLineWidth.setOnSeekBarChangeListener(new OnSeekBarChangeListener()
		    {
		       public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)  { }

		      public void onStartTrackingTouch(SeekBar seekBar) {}

		      public void onStopTrackingTouch(SeekBar seekBar)
		       {
		    	   Toast.makeText(getApplicationContext(), "新宽度 : " + seekBar.getProgress() + 1, Toast.LENGTH_SHORT).show();
		       }
		    });
	}

    public void colorpicker() {
  	  AmbilWarnaDialog dialog = new AmbilWarnaDialog(this, selColor, new OnAmbilWarnaListener() {
      	 
          // Executes, when user click Cancel button
          @Override 
          public void onCancel(AmbilWarnaDialog dialog){
          }

          // Executes, when user click OK button
          @Override
          public void onOk(AmbilWarnaDialog dialog, int color) {
        	  selColor = color;
          	 txtColorTip.setBackgroundColor(selColor);
         	 drawDes.setSelColor(selColor);
              Toast.makeText(getApplicationContext(), "您选择新颜色 : " + selColor, Toast.LENGTH_LONG).show();
          }
      });
      dialog.show();
  }

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.init__draw, menu);
		return true;
	}

}
