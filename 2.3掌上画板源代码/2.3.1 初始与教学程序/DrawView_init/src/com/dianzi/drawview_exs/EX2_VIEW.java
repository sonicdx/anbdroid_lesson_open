package com.dianzi.drawview_exs;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class EX2_VIEW extends View {
	public EX2_VIEW(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}
	Bitmap logoImg = BitmapFactory.decodeResource(getResources(), com.dianzi.drawview_init.R.drawable.logo);
	protected void onDraw(Canvas canvas) {
		//super.onDraw(canvas);
		canvas.drawColor(Color.BLUE);
		Paint p = new Paint();
		canvas.drawBitmap(logoImg, 0, 0, p);
	}
}
