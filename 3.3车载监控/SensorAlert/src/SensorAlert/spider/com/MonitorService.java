﻿package SensorAlert.spider.com;

import java.util.List;
import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;

public class MonitorService extends Service implements SensorEventListener{
    private SensorManager  mSM; 
    private boolean   mRegisteredSensor;


	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
  
	@Override
	public void onCreate()
	{
		super.onCreate();
		
		mSM= (SensorManager) getSystemService(SENSOR_SERVICE);  
		List<Sensor> sensors = mSM.getSensorList(Sensor.TYPE_ALL);
	      for (Sensor s : sensors) { 
	                mRegisteredSensor = mSM.registerListener((SensorEventListener) this, s, SensorManager.SENSOR_DELAY_FASTEST);
	             }
		

	}
	
	@Override 
     public void onDestroy(){
       if (mRegisteredSensor){ //注销Listener
                mSM.unregisterListener(this);
                mRegisteredSensor = false;
             }
     }
	 

	
	 
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
	
		  switch(event.sensor.getType()){
		  	case Sensor.TYPE_TEMPERATURE:CommData.curT=event.values[0];
		  		                         if(event.values[0]>CommData.HighT){
		  		                        	//If you don't want to use broadcast, please uncomment the following two lines 
		  		                        	/*NotificationMgr  nt=new NotificationMgr(getApplicationContext());
		  		                        	nt.useNotifi(getApplicationContext().getString(R.string.highTP), getApplicationContext().getString(R.string.currentT)+commData.curT );*/
		  		                        	
		  		                        	//If you want to use broadcast, please comment the following five lines
		  		                        	Intent mIntent = new Intent();
		  		                        	mIntent.setAction("SensorAlert.spider.com.ENVALERT");
		  		                        	mIntent.putExtra("title", getApplicationContext().getString(R.string.highTP));
		  		                        	mIntent.putExtra("content", getApplicationContext().getString(R.string.currentT)+CommData.curT);
		  		                        	this.sendBroadcast(mIntent);
		  		                         }
		  								  if(event.values[0]<CommData.LowT){
		  									//If you don't want to use broadcast, please uncomment the following two lines 
		  									/*NotificationMgr  nt=new NotificationMgr(getApplicationContext());
		  									nt.useNotifi(getApplicationContext().getString(R.string.lowTP), getApplicationContext().getString(R.string.currentT)+commData.curT );*/
			  		                        
		  									//If you want to use broadcast, please comment the following five lines
		  									Intent mIntent = new Intent();
		  									mIntent.setAction("SensorAlert.spider.com.ENVALERT");
			  		                        mIntent.putExtra("title", getApplicationContext().getString(R.string.lowTP));
			  		                        mIntent.putExtra("content", getApplicationContext().getString(R.string.currentT)+CommData.curT);
			  		                        this.sendBroadcast(mIntent);
		  								  }
		  								  break;
		  	case Sensor.TYPE_LIGHT:		CommData.light=event.values[0];
		  	 							break;
		  	case Sensor.TYPE_PROXIMITY:	CommData.distance=event.values[0];
		  	                            //If you don't want to use broadcast, please uncomment the following two lines 
		  								//NotificationMgr  nt=new NotificationMgr(getApplicationContext());
		  								//nt.useNotifi(getApplicationContext().getString(R.string.disAlarm), getApplicationContext().getString(R.string.curDistance)+commData.distance );
		  	
		  								//If you want to use broadcast, please comment the following five lines
          								Intent mIntent = new Intent();
          								mIntent.setAction("SensorAlert.spider.com.ENVALERT");
          								mIntent.putExtra("title", getApplicationContext().getString(R.string.disAlarm));
          								mIntent.putExtra("content", getApplicationContext().getString(R.string.curDistance)+CommData.distance+":"+CommData.HighT+":"+CommData.LowT);
          								this.sendBroadcast(mIntent);
		  								break;
		  	default:break;
		  }
	}
}
