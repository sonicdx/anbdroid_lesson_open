package com.dianzi.drawview_touch;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

public class TouchView extends View   {
	private int selColor = Color.BLACK;
	private float PosX = (float) -1.0;
	private float PosY = (float) -1.0;
	private float rad = (float)15.0;

	private int cavW = 0;
	private int cavH = 0;

	private Paint lineStyle = null;
	
	public TouchView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		
		lineStyle = new Paint();
		lineStyle.setAntiAlias(true);
		lineStyle.setColor(Color.RED);
		lineStyle.setStrokeWidth(2);
		lineStyle.setDither(true);
	    lineStyle.setStyle(Paint.Style.FILL);
	    lineStyle.setStrokeJoin(Paint.Join.ROUND);
	    lineStyle.setStrokeCap(Paint.Cap.ROUND);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		canvas.drawColor(selColor);

		if(PosX >=0.0 && PosY >=0)
		{
	    	canvas.drawCircle(PosX, PosY,rad,lineStyle);
			//canvas.drawLine((float)0.0,(float) 0.0, PosX, PosY, lineStyle)
		}
	}

	public int getSelColor() {
		return selColor;
	}

	public void setSelColor(int selColor) {
		this.selColor = selColor;
		this.invalidate();
	}

	@Override
	  public boolean onTouchEvent(MotionEvent event) {
	      float x = event.getX();
          float y = event.getY();

          switch (event.getAction()) {
              case MotionEvent.ACTION_DOWN:
                  touch_start(x, y);
                  invalidate();
                  break;
              case MotionEvent.ACTION_MOVE:
                  touch_move(x, y);
                  invalidate();
                  break;
              case MotionEvent.ACTION_UP:
                  touch_up();
                  invalidate();
                  break;
          }
          return true;
	}


     private void touch_start(float x, float y) {
    	 PosX = x;
    	 PosY = y;
     }
     private void touch_move(float x, float y) {
    	 PosX = x;
    	 PosY = y;
     }
     private void touch_up() {
    	 //PosX = (float) -1.0;
    	 //PosY = (float) -1.0;
     }
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		// TODO Auto-generated method stub
		super.onSizeChanged(w, h, oldw, oldh);
		
		this.cavW = w;
		this.cavH = h;
	}
	
	
	

}
