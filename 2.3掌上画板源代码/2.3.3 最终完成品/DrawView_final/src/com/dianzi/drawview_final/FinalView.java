package com.dianzi.drawview_final;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

public class FinalView extends View   {
	private int selColor = Color.BLACK;
	private int lineWidth = 3;

	private int cavW = 0;
	private int cavH = 0;
    private Paint lineStyle = null;

    private Bitmap  mBitmap = null;
    private Canvas  mCanvas = null;
    private Path    mPath = null;
    private Paint   mBitmapPaint;
    
	public FinalView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		
		lineStyle = new Paint();
		lineStyle.setAntiAlias(true);
		lineStyle.setColor(selColor);
		lineStyle.setStrokeWidth(2);
		lineStyle.setDither(true);
	    lineStyle.setStyle(Paint.Style.STROKE);
	    lineStyle.setStrokeJoin(Paint.Join.ROUND);
	    lineStyle.setStrokeCap(Paint.Cap.ROUND);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		if(mBitmap == null)
		{
	        mBitmap = Bitmap.createBitmap(cavW, cavH, Bitmap.Config.ARGB_8888);
	        mPath = new Path();
	        mBitmapPaint = new Paint(Paint.DITHER_FLAG);
	        mCanvas = new Canvas(mBitmap);
	        mCanvas.drawColor(Color.WHITE);
		}
		canvas.drawBitmap(mBitmap,0,0,mBitmapPaint);
		canvas.drawPath(mPath, lineStyle);
		
		//canvas.drawLine(0, 0, cavW, cavH, lineStyle);
	}

	public int getSelColor() {
		return selColor;
	}

	public void setSelColor(int selColor) {
		this.selColor = selColor;
		lineStyle.setColor(this.selColor);
		this.invalidate();
	}

	public int getLineWidth() {
		return lineWidth;
	}

	public void setLineWidth(int lineWidth) {
		this.lineWidth = lineWidth;
		lineStyle.setStrokeWidth(this.lineWidth);
		this.invalidate();
	}
	
	@Override
	  public boolean onTouchEvent(MotionEvent event) {
	      float x = event.getX();
          float y = event.getY();

          switch (event.getAction()) {
              case MotionEvent.ACTION_DOWN:
                  touch_start(x, y);
                  invalidate();
                  break;
              case MotionEvent.ACTION_MOVE:
                  touch_move(x, y);
                  invalidate();
                  break;
              case MotionEvent.ACTION_UP:
                  touch_up();
                  invalidate();
                  break;
          }
          return true;
	}
	 private float mX, mY;
     private static final float TOUCH_TOLERANCE = 4;

     private void touch_start(float x, float y) {
         mPath.reset();
         mPath.moveTo(x, y);
         mX = x;
         mY = y;
     }
     private void touch_move(float x, float y) {
         float dx = Math.abs(x - mX);
         float dy = Math.abs(y - mY);
         if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
             mPath.quadTo(mX, mY, (x + mX)/2, (y + mY)/2);
             mX = x;
             mY = y;
         }
     }
     private void touch_up() {
         mPath.lineTo(mX, mY);
         mCanvas.drawPath(mPath, lineStyle);
         mPath.reset();
     }
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		// TODO Auto-generated method stub
		super.onSizeChanged(w, h, oldw, oldh);
		
		this.cavW = w;
		this.cavH = h;
	}
	
	
	

}
