package com.test.mycalorie;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private ImageView myinfo;
	private ImageView todaycal; 
	private ImageView foodcheck;
	private ImageView help;
	private ImageView about;
	
	private Intent intent;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		myinfo=(ImageView)findViewById(R.id.imageView1);
		todaycal=(ImageView)findViewById(R.id.imageView3);
		foodcheck=(ImageView)findViewById(R.id.imageView4);
		help=(ImageView)findViewById(R.id.imageView5);
		about=(ImageView)findViewById(R.id.imageView6);
		
		myinfo.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				intent= new Intent();
				intent.setClass(getApplicationContext(), PersonActivity.class);
				startActivity(intent);
			}
		});
		
        todaycal.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				MyApplication app = ((MyApplication)getApplicationContext());
				if(app.getRee()!=0)
				{
				  intent= new Intent();
				  intent.setClass(getApplicationContext(), CalorieActivity.class);
				  startActivity(intent);
				}
				else
					Toast.makeText(getApplicationContext(), "请先到[健康报告]填写你的健康数据哦!", BIND_AUTO_CREATE).show();
			}
		});
        
        foodcheck.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				intent= new Intent();
				intent.setClass(getApplicationContext(), FoodActivity.class);
				startActivity(intent);
			}
		});
        
        help.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				intent= new Intent();
				intent.setClass(getApplicationContext(), HelpActivity.class);
				startActivity(intent);
			}
		});
        about.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog aboutDg;
			    aboutDg=new AlertDialog.Builder(MainActivity.this)
			                                          .setTitle("About Bingo")
			    		                              .setMessage("卡路里助手")
			    		                              .create();
				aboutDg.show();
			}
		});
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
	//	getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
