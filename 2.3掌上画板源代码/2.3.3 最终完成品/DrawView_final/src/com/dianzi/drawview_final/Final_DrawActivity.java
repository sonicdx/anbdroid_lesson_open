package com.dianzi.drawview_final;

import yuku.ambilwarna.AmbilWarnaDialog;
import yuku.ambilwarna.AmbilWarnaDialog.OnAmbilWarnaListener;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class Final_DrawActivity extends Activity {
	private Button btuChangeColor = null;
	private SeekBar seekLineWidth = null;
	private TextView txtColorTip = null;
	private  FinalView drawDes = null;
	
	private int lineColor = Color.BLACK; 
	private int lineWidth = 3;
	private Activity activity = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_final__draw);
	    activity = this;
		 
		btuChangeColor = (Button) findViewById(R.id.btuChangeColor);
		seekLineWidth = (SeekBar) findViewById(R.id.seekLineWidth);
		txtColorTip = (TextView)findViewById(R.id.color_tip); 
		drawDes = (FinalView)findViewById(R.id.finalView);
		
		txtColorTip.setBackgroundColor(lineColor);
		btuChangeColor.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				colorpicker();
			}
		});  
		seekLineWidth.setMax(19);
		seekLineWidth.setProgress(lineWidth -  1);
		seekLineWidth.setOnSeekBarChangeListener(new OnSeekBarChangeListener()
		    {
		       public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)  { }

		      public void onStartTrackingTouch(SeekBar seekBar) {}

		      public void onStopTrackingTouch(SeekBar seekBar)
		       {
		    	   lineWidth = seekBar.getProgress() + 1;
		    	   drawDes.setLineWidth(lineWidth);
		    	   Toast.makeText(getApplicationContext(), "新宽度 : " + lineWidth, Toast.LENGTH_SHORT).show();
		       }
		    });
		
	}
	
    public void colorpicker() {
    	  AmbilWarnaDialog dialog = new AmbilWarnaDialog(this, lineColor, new OnAmbilWarnaListener() {
        	 
            // Executes, when user click Cancel button
            @Override 
            public void onCancel(AmbilWarnaDialog dialog){
            }
 
            // Executes, when user click OK button
            @Override
            public void onOk(AmbilWarnaDialog dialog, int color) {
            	lineColor = color;
            	txtColorTip.setBackgroundColor(lineColor);
           	   drawDes.setSelColor(lineColor);
                Toast.makeText(getApplicationContext(), "您选择新颜色 : " + lineColor, Toast.LENGTH_LONG).show();
            }
        });
        dialog.show();
    }


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.final__draw, menu);
		return true;
	}

}
