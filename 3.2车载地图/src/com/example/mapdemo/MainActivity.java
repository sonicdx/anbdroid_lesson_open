package com.example.mapdemo;


import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.ViewGroup;

public class MainActivity extends MapActivity {

	private MapView map;
	private MapController mc;
	private ViewGroup zoom;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		map=(MapView)findViewById(R.id.map);
		zoom=(ViewGroup)findViewById(R.id.zoom);
		mc=map.getController();
		GeoPoint pos_stiei= new GeoPoint(
				(int)(30.926686*1000000),
				(int)(121.690796*1000000)
				);
		map.setSatellite(true);//启用卫星视图
		map.setTraffic(true);//启用一般地图
		map.setBuiltInZoomControls(true);//添加缩放图标
		mc.setZoom(19);//设置放大倍数（1-21）
		mc.animateTo(pos_stiei);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

}
