package com.dianzi.drawview_exs;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class EX5_VIEW extends View {

	public EX5_VIEW(Context context, AttributeSet attrs) {
		super(context, attrs);

	}
	RectF selRect = null;
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		// TODO Auto-generated method stub
		super.onSizeChanged(w, h, oldw, oldh);
		
		selRect = null;
		if(w == 0)
			return;
		
		selRect = new RectF(w/4,h/4,w/4 * 3,h/4 * 3);
		this.invalidate();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		//super.onDraw(canvas);
		canvas.drawColor(Color.WHITE);
		

		if(selRect != null)
		{
			Paint p = new Paint();
			if(isInRect == true)
				p.setColor(Color.BLUE);
			else
				p.setColor(Color.RED);
			
			p.setStyle(Style.FILL);
			canvas.drawRect(selRect, p);
		}
	}

	boolean isTouching = false;
	boolean isInRect = false;
	@Override
	  public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
            	isTouching = true;
            	CalIsInRect(event.getX(),event.getY());
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
            	CalIsInRect(event.getX(),event.getY());
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
            	isTouching = false;
            	isInRect = false;
                invalidate();
                break;
        }
        return true;
	}
	
	private void CalIsInRect(float mouseX,float mouseY)
	{
		if(selRect == null)
			isInRect = false;
		
		isInRect = selRect.contains(mouseX, mouseY);
	}
}
