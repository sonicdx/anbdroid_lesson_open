package com.dianzi.drawview_exs;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class EX6_VIEW extends View {
	public EX6_VIEW(Context context, AttributeSet attrs) {
		super(context, attrs);

	}
	
	RectF selRect = null;
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		// TODO Auto-generated method stub
		super.onSizeChanged(w, h, oldw, oldh);
		
		selRect = null;
		if(w == 0)
			return;
		
		selRect = new RectF(w/2 - 100,h/2 - 100,w/2 + 100,h/2 + 100);
		this.invalidate();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		//super.onDraw(canvas);
		canvas.drawColor(Color.WHITE);
		

		if(selRect != null)
		{
			Paint p = new Paint();
			if(isTouching == true)
				p.setColor(Color.BLUE);
			else
				p.setColor(Color.RED);
			
			p.setStyle(Style.FILL);
			canvas.drawRect(selRect, p);
		}
	}

	
	boolean isTouching = false;
	boolean isInRect = false;
	@Override
	  public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
            	isTouching = isCalIsInRect(event.getX(),event.getY());
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
            	if(isTouching == true)
            	{
                	selRect.left = event.getX() - 100;
                	selRect.top = event.getY() - 100;
                	selRect.right = event.getX() + 100;
                	selRect.bottom = event.getY() + 100;
                    invalidate();
            	}
                break;
            case MotionEvent.ACTION_UP:
            	isTouching = false;
                invalidate();
                break;
        }
        return true;
	}
	
	private boolean isCalIsInRect(float mouseX,float mouseY)
	{
		if(selRect == null)
			return false;
		
		return selRect.contains(mouseX, mouseY);
	}
}
