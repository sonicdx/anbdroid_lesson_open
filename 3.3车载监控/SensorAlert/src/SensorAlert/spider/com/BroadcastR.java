package SensorAlert.spider.com;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;


public class BroadcastR extends BroadcastReceiver {

	 
	public BroadcastR() {
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		Bundle bundle = intent.getExtras();
		 
		String title = bundle.getString("title");
		String content=bundle.getString("content");
		NotificationMgr  notifi=new NotificationMgr(context);
		notifi.useNotifi(title,content);
	}

}
