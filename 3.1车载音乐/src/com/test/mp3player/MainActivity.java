package com.test.mp3player;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	ContentResolver resolver;
	ListView audioList;
	Uri mp3uri;
	Cursor c;
	MediaPlayer mp;
	TextView playtitle;
	TextView timedis;
	Button playButton;
	Button pauseButton;
	Button stopButton;
	SeekBar timeSeek;
	
	Handler handler=new Handler();
	Runnable runnable;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		audioList = (ListView)findViewById(R.id.listView1);
		playtitle = (TextView)findViewById(R.id.textView1);
		timedis =(TextView)findViewById(R.id.textView2);
		timeSeek=(SeekBar)findViewById(R.id.seekBar1);
		playButton = (Button)findViewById(R.id.button1);
		pauseButton = (Button)findViewById(R.id.button2);
		stopButton = (Button)findViewById(R.id.button3);
		 
	    resolver = getContentResolver(); 
	    c = resolver.query( MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, null, null, null);
		
	    CursorAdapter adapter = new SimpleCursorAdapter(this,
	                android.R.layout.simple_list_item_2,c, 
	                new String[] {MediaStore.Audio.AudioColumns.TITLE,MediaStore.Audio.AudioColumns.ARTIST}, 
	                new int[] {android.R.id.text1,android.R.id.text2});
	        audioList.setAdapter(adapter);
	        runnable=new Runnable(){

				@Override
				public void run() {
					// TODO Auto-generated method stub
					String str;
					String str1,str2,str3;
					int songpos;
					int hours,minutes,seconds;
					
					songpos=mp.getCurrentPosition();
					hours=songpos/(1000*60*60);
					minutes=(songpos-hours*(1000*60*60))/(1000*60);
					seconds=(songpos-(hours*1000*60*60)-(minutes*1000*60))/1000;
					if(hours<10)
						str1="0"+hours;
					else
						str1=""+hours;
					if(minutes<10)
						str2="0"+minutes;
					else
					    str2=""+minutes;
					if(seconds<10)
						str3="0"+seconds;
					else
						str3=""+seconds;
					str=str1+":"+str2+":"+str3;
					timedis.setText(str);
					timeSeek.setProgress(mp.getCurrentPosition());
					handler.postDelayed(this, 1000);
					
				}};
	        
	        audioList.setOnItemLongClickListener(new OnItemLongClickListener(){

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				
				//Toast.makeText(getApplicationContext(), "ok", BIND_AUTO_CREATE).show();
				String str;
				
				
				mp3uri = Uri.withAppendedPath(
                        MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, 
                        "/"+String.valueOf(arg3));
				
		
		       c.moveToPosition(arg2);
		     
		       int currentapiVersion=android.os.Build.VERSION.SDK_INT;
		       if(currentapiVersion<11)
		       str=c.getString(7);//get Title
		       else
		       str=c.getString(8);//get Title
		     	//	Toast.makeText(getApplicationContext(), str, BIND_AUTO_CREATE).show();
			   playtitle.setText(str);
			  // mp.release();
			   mp=MediaPlayer.create(getApplicationContext(), mp3uri);
			   timeSeek.setMax(mp.getDuration());
				
				
				return false;
			}});
	      
	      playButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(mp.isPlaying()==false)
			   {
					 mp.start();
				     handler.postDelayed(runnable, 1000);
			 }
			}
		});
	      pauseButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(mp.isPlaying()==true)
					   mp.pause();
			}
		});
	      
	      stopButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(mp.isPlaying()==true)
					   {mp.stop();
				       handler.removeCallbacks(runnable);} 
			}
		});
	      
	      timeSeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
				
			}
		});
	      
	      
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
