package SensorAlert.spider.com;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.IBinder;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import SensorAlert.spider.com.NotificationMgr;

public class MainActivity extends Activity {
RadioGroup  serviceLaunchRadioGroup;
RadioButton serviceStartRadio;
RadioButton serviceStopRadio;
EditText    etHigh,etLow;     


		@Override
		public void onCreate(Bundle savedInstanceState)
		{
			super.onCreate(savedInstanceState);
			setContentView(R.layout.main);
			serviceLaunchRadioGroup= (RadioGroup) findViewById(R.id.serviceLaunchRadioGroup);
			serviceStartRadio = (RadioButton) findViewById(R.id.serviceStartRadio);  
			serviceStopRadio = (RadioButton) findViewById(R.id.serviceStopRadio);
			etHigh = (EditText) findViewById(R.id.editText1);
			etLow = (EditText) findViewById(R.id.editText2);
		
			serviceLaunchRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() { 	  
			public void onCheckedChanged(RadioGroup group, int checkedId) {  
			        if (checkedId == serviceStartRadio.getId()) {
			        	Intent intent=new Intent(MainActivity.this, MonitorService.class); 
			        	startService(intent);
			        	CommData.serviceStatus=true;
			            //Toast.makeText(getApplicationContext(), R.string.serviceLaunched,Toast.LENGTH_LONG); 

			        } else if (checkedId == serviceStopRadio.getId()) {  
			        	Intent intent=new Intent(MainActivity.this, MonitorService.class); 
			        	stopService(intent);
			        	NotificationMgr  nt=new NotificationMgr(getApplicationContext());
						nt.clear();
						CommData.serviceStatus=false;
			        	//Toast.makeText(getApplicationContext(), R.string.serviceStopped,Toast.LENGTH_LONG);
			        
			        }  
			    }  
			}); 
			
		    etHigh.addTextChangedListener(textWatcherH);
		    etLow.addTextChangedListener(textWatcherL);
			

		}
		
		 private TextWatcher textWatcherH = new TextWatcher() {

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				CommData.HighT=new Float(arg0.toString());
			}  
			 
		 };
		 
		 private TextWatcher textWatcherL = new TextWatcher() {

				@Override
				public void afterTextChanged(Editable arg0) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void beforeTextChanged(CharSequence arg0, int arg1,
						int arg2, int arg3) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onTextChanged(CharSequence arg0, int arg1, int arg2,
						int arg3) {
					// TODO Auto-generated method stub
					CommData.LowT=new Float(arg0.toString());
				}  
				 
			 };
		
		 @Override
	     public void onResume(){  
			 super.onResume();
			 if(CommData.serviceStatus==true){
				 serviceStartRadio.setChecked(true);
			 }else{
				 serviceStartRadio.setChecked(false);
			 }
		 }
}
