package com.test.mycalorie;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

public class CalorieActivity extends Activity {

	private TextView reetext;
	private TextView todayree;
	private Button addbt;
	private ImageView iv1;
	private ImageView iv2;
	private ImageView iv3;
	private ImageView iv4;
	private ImageView iv5;
	private ImageView iv6;
	private ImageView iv7;
	private Spinner sp;
	private TextView foodtext;
	private RadioGroup rg;
	
	private String type;
	private SQLiteDatabase db;
	private String dbstr;
	private Cursor cur;
	private ArrayAdapter<String> adapter;	
	private List< String> list = new ArrayList< String>();
	private String foodstr;
	private int cal;
	private int calsum;
	private float ree;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_calorie);
		
		reetext = (TextView)findViewById(R.id.textView1);
		todayree = (TextView)findViewById(R.id.textView2);
		addbt = (Button)findViewById(R.id.button1);
		iv1 = (ImageView)findViewById(R.id.imageView3);
		iv2 = (ImageView)findViewById(R.id.imageView4);
		iv3 = (ImageView)findViewById(R.id.imageView5);
		iv4 = (ImageView)findViewById(R.id.imageView6);
		iv5 = (ImageView)findViewById(R.id.imageView7);
		iv6 = (ImageView)findViewById(R.id.imageView8);
		iv7 = (ImageView)findViewById(R.id.imageView9);
		sp = (Spinner)findViewById(R.id.spinner1);
		foodtext = (TextView)findViewById(R.id.textView3);
		rg = (RadioGroup)findViewById(R.id.radioGroup1);
		
		list.add("");
		adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,list);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); 
		sp.setAdapter(adapter);
		
		type = "肉类";
		calsum = 0;
		foodstr="";
	//	ree=1648;
		MyApplication app = ((MyApplication)getApplicationContext());   
		ree=app.getRee();
        reetext.setText(String.format("每天应摄于约%.2f大卡热量", ree));    
        todayree.setText("今日已摄入");
        
        iv1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				type = "肉类";
				selectitem(type);
			}
		});
        iv2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				type = "蔬菜";
				selectitem(type);
			}
		});
        iv3.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				type = "主食";
				selectitem(type);
			}
		});
        iv4.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				type = "水产";
				selectitem(type);
			}
		});
        iv5.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				type = "蛋类";
				selectitem(type);
			}
		});
        iv6.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				type = "豆制品";
				selectitem(type);
			}
		});
        iv7.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				type = "水果";
				selectitem(type);
			}
		});
        addbt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			String foodname;
			int n=1;
	
			foodname = sp.getSelectedItem().toString();
			
			switch(rg.getCheckedRadioButtonId())
			{
			  case R.id.radio0:n=1;break;
			  case R.id.radio1:n=2;break;
			  case R.id.radio2:n=3;break;
			  case R.id.radio3:n=4;break;
			  case R.id.radio4:n=5;
			}
			
			if(foodname.length()>0)
			{
				selectcal(foodname);
				calsum+=(cal*n);
				foodstr=foodstr+foodname+"  ";
				todayree.setText("今日已摄入"+String.valueOf(calsum)+"大卡热量");
				foodtext.setText(foodstr);
				if(calsum>ree)
				{
					
					todayree.setText("摄入热量已超标");
					
					
				}
			}
			
				
		 }
		});
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
	//	getMenuInflater().inflate(R.menu.calorie, menu);
		return true;
	}
	
	public void selectitem(String type)
	{
		int count = 0;
		dbstr=getApplicationContext().getApplicationInfo().dataDir;
		dbstr=dbstr+"/databases/food.db";
		db= SQLiteDatabase.openDatabase(dbstr, null, 1) ;
		cur=db.query(true,"foods", new String[]{"name"}, "type=?", 
				new String[]{type}, null, null, null,null);
		
		count = cur.getCount();
		list.clear();
		if(count>0)
		{
			cur.moveToFirst();
			for(int i=0;i<count;i++)
			{
				list.add(cur.getString(0));
				cur.moveToNext();
			}
			
			adapter.notifyDataSetChanged();
		}
		cur.close();
		db.close();
	}
	
	public void selectcal(String name)
	{
		int count = 0;
		dbstr=getApplicationContext().getApplicationInfo().dataDir;
		dbstr=dbstr+"/databases/food.db";
		db= SQLiteDatabase.openDatabase(dbstr, null, 1) ;
		cur=db.query(true,"foods", new String[]{"heatkilocalorie"}, "name=?", 
				new String[]{name}, null, null, null,null);
		
		count = cur.getCount();
	//	list.clear();
		if(count>0)
		{
			cur.moveToFirst();
	//		for(int i=0;i<count;i++)
	//		{
	//			list.add(cur.getString(0));
			cal = cur.getInt(0);
	//			cur.moveToNext();
	//		}
			
			adapter.notifyDataSetChanged();
		}
		cur.close();
		db.close();
	}

}
