package com.dianzi.drawview_exs;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

public class EX3_VIEW extends View {

	public EX3_VIEW(Context context, AttributeSet attrs) {
		super(context, attrs);

	}

	int PolyCount = 0;
	Bitmap logoImg = BitmapFactory.decodeResource(getResources(), com.dianzi.drawview_init.R.drawable.logo);
	@Override
	protected void onDraw(Canvas canvas) {
		//super.onDraw(canvas);
		canvas.drawColor(Color.BLUE);
		
		Paint p = new Paint();
		DrawNPolyon(this.getWidth() / 2,this.getHeight() / 2,PolyCount + 3,150,getContext(),canvas);
	}

	
	boolean isTouching = false;
	@Override
	  public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
            	isTouching = true;
            	PolyCount = (PolyCount + 1) % 20;
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
            	isTouching = false;
                invalidate();
                break;
        }
        return true;
	}
	
	protected void DrawNPolyon(int x,int y,int n,int r,Context caller,Canvas drawer)
	{
		if(n < 3)
		{
			Toast.makeText(caller, "不能绘制边数小于3的多边形", Toast.LENGTH_SHORT).show();
			return;
		}
		
		Paint p = new Paint();
		p.setStrokeWidth(4);
		p.setColor(Color.RED);
		double perAng = Math.PI * 2 / n;
		for(int i=0;i<n;i++)
		{
			int startIndex = i;
			int endIndex = (i + 1) % n;
			float startX = (float)x + (float)(r * Math.sin(startIndex * perAng));
			float startY = (float)y + (float)(r * Math.cos(startIndex * perAng));
			float stopX = (float)x + (float)(r * Math.sin(endIndex * perAng));
			float stopY = (float)y + (float)(r * Math.cos(endIndex * perAng));
			drawer.drawLine(startX, startY, stopX, stopY, p);
		}	
	}
}
