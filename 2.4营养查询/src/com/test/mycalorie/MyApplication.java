package com.test.mycalorie;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

public class MyApplication extends Application{

	public static String dbName="food.db";//数据库的名字
	private static String DATABASE_PATH="/data/data/com.test.mycalorie/databases/";//数据库在手机里的路径
	private float ree;
	
//	private String[] breakfast;
//	private String[] lunch;
//	private String[] dinner;
	
//	private float[] cal1;
//	private float[] cal2;
//	private float[] cal3;
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		
		boolean dbExist = checkDataBase();
        if(dbExist){
        }else{//不存在就把raw里的数据库写入手机
            try{
                copyDataBase();
            }catch(IOException e){
                throw new Error("Error copying database");
            }
        }  
        
        
	}
	
	public float getRee(){    
		    return ree;    
		   }    
	
	public void setRee(float f){    
		    ree = f;    
		  }    


	
	public boolean checkDataBase(){
        SQLiteDatabase checkDB = null;
        try{
            String databaseFilename = DATABASE_PATH+dbName;
            checkDB =SQLiteDatabase.openDatabase(databaseFilename, null,
                    SQLiteDatabase.OPEN_READONLY);
        }catch(SQLiteException e){          
        }
        if(checkDB!=null){
            checkDB.close();
        }
        return checkDB !=null?true:false;     
        }
	
	
	public void copyDataBase() throws IOException{
        String databaseFilenames =DATABASE_PATH+dbName;
        File dir = new File(DATABASE_PATH);
        if(!dir.exists())//判断文件夹是否存在，不存在就新建一个
            dir.mkdir(); 
        FileOutputStream os = null;
        try{
           os = new FileOutputStream(databaseFilenames);//得到数据库文件的写入流
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }
        InputStream is = this.getResources().openRawResource(R.raw.food);//得到数据库文件的数据流 
        byte[] buffer = new byte[8192];
        int count = 0;
        try{
            while((count=is.read(buffer))>0){
                os.write(buffer, 0, count);
                os.flush();
            }
        }catch(IOException e){
        }
        try{
            is.close();
            os.close();
        }catch(IOException e){
            e.printStackTrace();
    }




}
	
}
