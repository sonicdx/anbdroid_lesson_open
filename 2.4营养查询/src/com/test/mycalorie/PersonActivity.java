package com.test.mycalorie;



import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class PersonActivity extends Activity {

	private Button bt1;
	private Button bt2;
	private Button bt3;
	private Button ok;
//	private EditText et; 
	
	private RadioButton rbt1;
	private RadioButton rbt2;
	private TextView heighttext;
	private TextView weighttext;
	private TextView agetext;
	private TextView kgtext;
	
	private TextView bmitext;
	private TextView fattext;
	private TextView reetext;
	
	private TextView bmiresult;
	
	private ImageView iv1;
	private ImageView iv2;
	private ImageView iv3;
	private ImageView iv4;
	
	private AlertDialog inputDg;
	
	private int height;
	private float weight;
	private int age;
	private boolean sex;
	
	private float bmi;
	private int fatl;
	private int fath;
	private float ree;
	private float kgl;
	private float kgh;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_person);
		
		rbt1=(RadioButton)findViewById(R.id.radioButton1);
		rbt2=(RadioButton)findViewById(R.id.radioButton2);
		
		bt1=(Button)findViewById(R.id.button1);
		bt2=(Button)findViewById(R.id.button2);
		bt3=(Button)findViewById(R.id.button3);
		ok=(Button)findViewById(R.id.button4);
		
		iv1=(ImageView)findViewById(R.id.imageView3);
		iv2=(ImageView)findViewById(R.id.imageView4);
		iv3=(ImageView)findViewById(R.id.imageView5);
		iv4=(ImageView)findViewById(R.id.imageView6);
		
		heighttext=(TextView)findViewById(R.id.textView3);
		weighttext=(TextView)findViewById(R.id.textView5);
		agetext=(TextView)findViewById(R.id.textView7);

		bmitext=(TextView)findViewById(R.id.textView11);
		fattext=(TextView)findViewById(R.id.textView13);
		reetext=(TextView)findViewById(R.id.textView8);
		kgtext=(TextView)findViewById(R.id.textView9);
		
		bmiresult=(TextView)findViewById(R.id.textView12);
		
		sex=true;
		height=0;
		weight=0;
		age=0;
		rbt1.setChecked(true);
		rbt2.setChecked(false);
		
		rbt1.setOnCheckedChangeListener(new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
		        if(isChecked)
				   {rbt2.setChecked(false);
		            sex=true;}
		        else
				   rbt2.setChecked(true);
				
			}});
		
		rbt2.setOnCheckedChangeListener(new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked)
				   {rbt1.setChecked(false);
		            sex=false;}
		        else
				   rbt1.setChecked(true);
			}});
		
		bt1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				final EditText et= new EditText(PersonActivity.this);
				
				inputDg = new AlertDialog.Builder(PersonActivity.this)  
				              .setTitle("请输入身高(cm)")  
				              .setIcon(android.R.drawable.ic_dialog_info)  
				              .setView(et)  
				              .setPositiveButton("确定", new DialogInterface.OnClickListener(){

					            @Override
					            public void onClick(DialogInterface arg0, int arg1) {
						        // TODO Auto-generated method stub
					               String str;
						           str=et.getText().toString();
					               if(!str.equals(""))
						           {
					            	 if(isNumber(str))
					            	 {
					            	  height=Integer.valueOf(et.getText().toString());
						              heighttext.setText(et.getText().toString()+"cm");
					            	 }
						           }
					               else
					            	   Toast.makeText(getApplicationContext(), "请输入正确数据", BIND_AUTO_CREATE).show();
						
					            }
					
				              })  
				             .setNegativeButton("取消", null)  
				             .show();  
				
                }
			 
		});
        
		bt2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final EditText et= new EditText(PersonActivity.this);
				inputDg = new AlertDialog.Builder(PersonActivity.this)  
				              .setTitle("请输入体重(kg)")  
				              .setIcon(android.R.drawable.ic_dialog_info)  
				              .setView(et)  
				              .setPositiveButton("确定", new DialogInterface.OnClickListener(){

					            @Override
					            public void onClick(DialogInterface arg0, int arg1) {
						        // TODO Auto-generated method stub
					            String str;
							    str=et.getText().toString();
						        if(!str.equals(""))
							    {
						           if(isNumber(str))
						           {
					                  weight=Integer.valueOf(et.getText().toString());
						              weighttext.setText(et.getText().toString()+"kg");
							       }
							    }
						        else
					            	   Toast.makeText(getApplicationContext(), "请输入正确数据", BIND_AUTO_CREATE).show();
						
					          }
					
				              })  
				             .setNegativeButton("取消", null)  
				             .show();  
				
                }
			
		});
        
        bt3.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final EditText et= new EditText(PersonActivity.this);
				inputDg = new AlertDialog.Builder(PersonActivity.this)  
				              .setTitle("请输入年龄")  
				              .setIcon(android.R.drawable.ic_dialog_info)  
				              .setView(et)  
				              .setPositiveButton("确定", new DialogInterface.OnClickListener(){

					            @Override
					            public void onClick(DialogInterface arg0, int arg1) {
						        // TODO Auto-generated method stub
					            String str;
								str=et.getText().toString();
							    if(!str.equals(""))
								{
							        if(isNumber(str))
							        {
						              age=Integer.valueOf(et.getText().toString());
						              agetext.setText(et.getText().toString()+"岁");
							        }
								}
							    else
					            	   Toast.makeText(getApplicationContext(), "请输入正确数据", BIND_AUTO_CREATE).show();
						
					           }
					
				              })  
				             .setNegativeButton("取消", null)  
				             .show();  
			}
		});
        
        ok.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(weight!=0&&height!=0)
				{
				  bmi=(float)(1.3*(weight)/Math.pow(height/100.0, 2.5));
				  bmitext.setText(String.format(" %.2f", bmi));
				  if(bmi<22.5&&bmi>21.5)
						bmiresult.setText("你的体重非常标准!");
					else
						if(bmi<=18.5)
						   bmiresult.setText("你的体重过轻,注意增加营养哦!");
						else
							if(bmi>18.5&&bmi<20)
								bmiresult.setText("你的体重较轻，要注意哦!");
							else
								if((bmi>=20&&bmi<=21.5)||(bmi>=22.5&&bmi<25))
									bmiresult.setText("你的体重适中，要保持哦!");
								else
									if(bmi>=25&&bmi<30)
										bmiresult.setText("你的体重过重，要注意减肥哦!");
									else
										if(bmi>=30&&bmi<35)
											bmiresult.setText("你的已经肥胖，一定要减肥哦!");
										else
											if(bmi>=35)
												bmiresult.setText("你已经非常肥胖，请立即减肥!");
				    kgl=(float)(18*(height/100.0)*(height/100.0));
					kgh=(float)(24*(height/100.0)*(height/100.0));
					kgtext.setText(String.format(" %.1f",kgl)+"Kg-"+String.format("%.1f",kgh)+"Kg");
				}
				else
					Toast.makeText(getApplicationContext(), "请输入身高和体重", BIND_AUTO_CREATE).show();
				
				
				if(age!=0)
				{
				  fatl=(int)((220-age)*0.6);
				  fath=(int)((220-age)*0.8);
				  fattext.setText(" "+String.valueOf(fatl)+"次/分-"+String.valueOf(fath)+"次/分");
				}
				else
					Toast.makeText(getApplicationContext(), "请输入年龄", BIND_AUTO_CREATE).show();
				
				if(height!=0&&weight!=0&&age!=0)
				{
				
				  if(sex)
					 ree = (float)((10*weight)+(6.25*height)-(5*age)+5);
				  else
					 ree = (float)((10*weight)+(6.25*height)-(5*age)+161);
				  MyApplication app = ((MyApplication)getApplicationContext());    
		          app.setRee(ree);    
		          reetext.setText(String.format(" %.2f大卡", ree));
				}
				else
					Toast.makeText(getApplicationContext(), "请输入各项数据", BIND_AUTO_CREATE).show();
				
				
				
				
				
			}
		});
        
        iv1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog BMIDg;
			    BMIDg=new AlertDialog.Builder(PersonActivity.this)
			                                          .setTitle("BMI")
			    		                              .setMessage("BMI指数(身体质量指数)\n是目前国际上常用的衡量人体胖瘦程度以及是否健康的一个标准。主要用于统计用途，当我们需要比较及分析一个人的体重对于不同高度的人所带来的健康影响时，BMI值是一个中立而可靠的指标。")
			    		                              .create();
				BMIDg.show();
			}
		});
        
        iv2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog FATDg;
			    FATDg=new AlertDialog.Builder(PersonActivity.this)
			                                          .setTitle("FAT")
			    		                              .setMessage("FAT指数(燃脂运动心率)\n适合你的中低强度运动心率，低于这个范围或者高于这个范围，都不是以燃烧脂肪供能为主，减脂效果略差。如需达到良好的减肥效果则你的运动需要满足下面三个必要条件：\n1、该运动要达到中低强度的运动心率；\n2、这种中低强度运动心率的运动要持续20分钟以上；\n3、这种运动必须是大肌肉群的运动，如慢跑、游泳、健身操等。")
			    		                              .create();
				FATDg.show();
			}
		});
        
        iv3.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog FATDg;
			    FATDg=new AlertDialog.Builder(PersonActivity.this)
			                                          .setTitle("REE")
			    		                              .setMessage("REE指数(静态能量消耗值)\n是指一般人所需的最小熱能需求量。一般作为 减肥节食每天热量摄入的警告值。 ")
			    		                              .create();
				FATDg.show();
			}
		});
		
        iv4.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog FATDg;
			    FATDg=new AlertDialog.Builder(PersonActivity.this)
			                                          .setTitle("标准体重")
			    		                              .setMessage("标准体重指数\n从您的身高计算出的合理体重范围，做为您健身减肥的参靠依据。 ")
			    		                              .create();
				FATDg.show();
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.person, menu);
		return true;
	}
	
	public static boolean isNumber(String str){  
		   
	    char c;
	    if(str!=null){  
	        for(int i=0;i<str.length();i++){  
	           c = str.charAt(i);  
	           if(Character.isDigit(c)==false){  
	               return false;   
	            }  
	           
	        }  
	        return true;
	    } 
	    else
	      return false;	      
	 
	}  

}
