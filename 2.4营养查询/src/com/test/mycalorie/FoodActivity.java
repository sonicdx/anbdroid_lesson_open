package com.test.mycalorie;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.os.Bundle;
import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.Toast;

public class FoodActivity extends Activity {

	private ImageView iv1;
	private ImageView iv2;
	private ImageView iv3;
	private ImageView iv4;
	private ImageView iv5;
	private ImageView iv6;
	private ImageView iv7;
	private Spinner sp;
	private Button bt;
	private ListView lv;
	
	private String type;
	private SQLiteDatabase db;
	private String dbstr;
	private Cursor cur;
	private ArrayAdapter<String> adapter;	
	private List< String> list = new ArrayList< String>();
	private SimpleAdapter foodadapter;
	private ArrayList<Map<String,String>> foodlist = new ArrayList<Map<String,String>>();
	private Map<String,String> map;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_food);
		
		iv1 = (ImageView)findViewById(R.id.imageView1);
		iv2 = (ImageView)findViewById(R.id.imageView2);
		iv3 = (ImageView)findViewById(R.id.imageView3);
		iv4 = (ImageView)findViewById(R.id.imageView4);
		iv5 = (ImageView)findViewById(R.id.imageView5);
		iv6 = (ImageView)findViewById(R.id.imageView6);
		iv7 = (ImageView)findViewById(R.id.imageView7);
		sp = (Spinner)findViewById(R.id.spinner1);
		bt = (Button)findViewById(R.id.button1);
		lv = (ListView)findViewById(R.id.listView1);
		
		type = "肉类";
		
		map = new HashMap<String,String>();
		
		list.add("");
		adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,list);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); 
		sp.setAdapter(adapter);
		
		map.put("item", "无");
		map.put("value", "无");
		foodadapter = new SimpleAdapter(this,foodlist,android.R.layout.simple_list_item_2,
                new String[]{"value","item"},
                new int[]{android.R.id.text1,android.R.id.text2});

        lv.setAdapter(foodadapter);
		
		iv1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				type = "肉类";
				selectitem(type);
				
			}
		});
        iv2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				type = "蔬菜";
				selectitem(type);
				
			}
		});
        iv3.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				type = "主食";
				selectitem(type);
				
			}
		});
        iv4.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				type = "水产";
				selectitem(type);
				
			}
		});
        iv5.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				type = "蛋类";
				selectitem(type);
				
			}
		});
        iv6.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				type = "豆制品";
				selectitem(type);
				
			}
		});
        iv7.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				type = "水果";
				selectitem(type);
				
			}
		});
		
		bt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				String foodname;
				
				foodname = sp.getSelectedItem().toString();
			
				if(foodname.length()>0)
				{
					selectcal(foodname);
					
				}
				else
					Toast.makeText(getApplicationContext(), "请先选择食物!", BIND_AUTO_CREATE).show();
			}
		});
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.food, menu);
		return true;
	}
	
	public void selectitem(String type)
	{
		int count = 0;
		dbstr=getApplicationContext().getApplicationInfo().dataDir;
		dbstr=dbstr+"/databases/food.db";
		db= SQLiteDatabase.openDatabase(dbstr, null, 1) ;
		cur=db.query(true,"foods", new String[]{"name"}, "type=?", 
				new String[]{type}, null, null, null,null);
		
		count = cur.getCount();
		list.clear();
		if(count>0)
		{
			cur.moveToFirst();
			for(int i=0;i<count;i++)
			{
				list.add(cur.getString(0));
				cur.moveToNext();
			}
			
			adapter.notifyDataSetChanged();
		}
		cur.close();
		db.close();
	}
	
	public void selectcal(String name)
	{
		int count = 0;
		dbstr=getApplicationContext().getApplicationInfo().dataDir;
		dbstr=dbstr+"/databases/food.db";
		db= SQLiteDatabase.openDatabase(dbstr, null, 1) ;
		cur=db.query(true,"foods", new String[]{"fat","protein","heatkilocalorie","carbohydrate","sodium","calcium","iron","zinc"}, "name=?", 
				new String[]{name}, null, null, null,null);
		
		count = cur.getCount();
		foodlist.clear();
		
		if(count>0)
		{
			if(cur.getColumnCount()==8)
			{
			  map = new HashMap<String,String>();
			  cur.moveToFirst();
			  map.put("value",String.format("%.2f克",cur.getFloat(0)));
			  map.put("item", "脂肪含量");
			  foodlist.add(map);
			  map = new HashMap<String,String>();
			  map.put("value",String.format("%.2f克",cur.getFloat(1)));
			  map.put("item", "蛋白质含量");
			  foodlist.add(map);
			  map = new HashMap<String,String>();
			  map.put("value",String.format("%d大卡",cur.getInt(2)));
			  map.put("item", "卡路里含量");
			  foodlist.add(map);
			  map = new HashMap<String,String>();
			  map.put("value",String.format("%.2f克",cur.getFloat(3)));
			  map.put("item", "碳水化合物含量");
			  foodlist.add(map);
			  map = new HashMap<String,String>();
			  map.put("value",String.format("%.2f毫克",cur.getFloat(4)));
			  map.put("item", "钠元素含量");
			  foodlist.add(map);
			  map = new HashMap<String,String>();
			  map.put("value",String.format("%.2f毫克",cur.getFloat(5)));
			  map.put("item", "钙元素含量");
			  foodlist.add(map);
			  map = new HashMap<String,String>();
			  map.put("value",String.format("%.2f毫克",cur.getFloat(6)));
			  map.put("item", "铁元素含量");
			  foodlist.add(map);
			  map = new HashMap<String,String>();
			  map.put("value",String.format("%.2f毫克",cur.getFloat(7)));
			  map.put("item", "锌元素含量");
			  foodlist.add(map);
	
			
			  foodadapter.notifyDataSetChanged();
			}
		}
		cur.close();
		db.close();
	}

}
